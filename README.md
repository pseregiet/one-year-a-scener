# One Year a Scener

A small Commodore64 intro celebrating my one year on the scene. It fits into 16kb ($4000-$7fff) and is PAL/NTSC compatible.

Code by Golara
Sprite font by Bokanoid
Music by Jeff

License ? Meh, public domain or something.. just don't change my name to yours and call it your intro :)


# How to build?

You need KickAssembler. Uncomment 17th line if you don't want to compress the binary and just run it.
If you want to compress it with self extracting exomizer use this command:
exomizer sfx 0x6c18 uncompressed-binary.prg -o compressed-binary.prg

0x6c18 is the entry point of this intro
