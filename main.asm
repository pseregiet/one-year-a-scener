.var muzak = LoadSid("Commodore_64_7200.sid")
.const scroller_max_sprites = 15
* = $02 "ZP" virtual
.zp{
    scrollorder:    .fill scroller_max_sprites+1, $00
    scrollbuffer:   .fill scroller_max_sprites, $00
    scrollypos:     .fill scroller_max_sprites, $00
    scrollxpos:     .fill scroller_max_sprites, $00
    scrollxmsb:     .fill scroller_max_sprites, $00
    scrollptr:      .fill 2,  $00
    zptmp1:         .fill 1,  $00
    spriteyoffset:  .fill 1,  $00
}



//:BasicUpstart2(entry)

* = $4000 "VIC: SPRITE FONT"
font:
.var spritefont = LoadPicture("gfx/golarafont.png",List().add(
    $7abfc7,$000000,$ffffff,$ababab
))
.for (var x = 0; x < 64; x++)
{
    .for (var y = 0; y < 21; y++)
    {
        .byte spritefont.getMulticolorByte(x*3+0, y)
        .byte spritefont.getMulticolorByte(x*3+1, y)
        .byte spritefont.getMulticolorByte(x*3+2, y)
    }
    .byte $00
}
* = $5000 "VIC: CHARSET"
//GOLARA LOGO (12 CHARLINES)
.import binary "gfx/charset.bin",2
//ONE YEAR A SCENER (2 CHARLINES)
{
.var oy = LoadPicture("gfx/oneyear.bmp")
.for (var y = 0; y < 2; y++)
{
    .for (var x = 0; x < 25; x++)
    {
        .fill 8, oy.getSinglecolorByte(x, y*8+i)
    }
}
}
* = $5800 "VIC: SCREEN"
screenptr:
//empty line
.fill 40, $00
//one year a scener(2chars)
.for (var c = 0; c < 2; c++)
{
.fill 7, $00
.fill 25, $b0 + i + c*25
.fill 8, $00
}
//empty line
.fill 40, $00
//12 chars for logo
.fill 12*40, $00
//empty line
.fill 40, $00
//one year a scener(2chars)
.for (var c = 0; c < 2; c++)
{
.fill 7, $00
.fill 25, $b0 +00 + i + c*25
.fill 8, $00
}


* = $5c00 "tables"
fld_tab:
.byte 16,16,15,15,14,14,14,13,13,13,12,12,11,11,11,10
.byte 10,10,9,9,9,8,8,8,7,7,7,6,6,6,6,5
.byte 5,5,4,4,4,4,4,3,3,3,3,2,2,2,2,2
.byte 2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1
.byte 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2
.byte 2,2,2,2,2,3,3,3,3,3,4,4,4,4,5,5
.byte 5,5,6,6,6,7,7,7,8,8,8,8,9,9,10,10
.byte 10,11,11,11,12,12,12,13,13,14,14,14,15,15,15,16
.byte 16,17,17,17,18,18,18,19,19,20,20,20,21,21,21,22
.byte 22,22,23,23,24,24,24,24,25,25,25,26,26,26,27,27
.byte 27,27,28,28,28,28,29,29,29,29,29,30,30,30,30,30
.byte 30,31,31,31,31,31,31,31,31,31,31,31,31,31,31,31
.byte 31,31,31,31,31,31,31,31,31,31,31,31,31,31,30,30
.byte 30,30,30,30,30,29,29,29,29,28,28,28,28,28,27,27
.byte 27,26,26,26,26,25,25,25,24,24,24,23,23,23,22,22
.byte 22,21,21,21,20,20,19,19,19,18,18,18,17,17,16,16


.var logosinus = List().add(
 356,347,338,329,320,312,303,294,286,277,269,260,252,243,235,227
,219,211,203,195,187,179,172,164,157,150,143,136,129,122,116,109
,103,97,91,85,80,74,69,64,59,54,49,45,41,37,33,29
,26,23,20,17,14,12,10,8,6,4,3,2,1,0,0,0
,0,0,0,1,1,2,4,5,7,9,11,13,16,18,21,24
,28,31,35,39,43,47,52,56,61,66,71,77,82,88,94,100
,106,112,119,126,132,139,146,153,161,168,176,183,191,199,207,215
,223,231,239,248,256,264,273,281,290,299,307,316,325,334,342,351
,360,369,377,386,395,404,412,421,430,438,447,455,463,472,480,488
,496,504,512,520,528,535,543,550,558,565,572,579,585,592,599,605
,611,617,623,629,634,640,645,650,655,659,664,668,672,676,680,683
,687,690,693,695,698,700,702,704,706,707,709,710,710,711,711,711
,711,711,711,710,709,708,707,705,703,701,699,697,694,691,688,685
,682,678,674,670,666,662,657,652,647,642,637,631,626,620,614,608
,602,595,589,582,575,568,561,554,547,539,532,524,516,508,500,492
,484,476,468,459,451,442,434,425,417,408,399,391,382,373,364,356
) 
.align $100
logoxinelo:
.fill $100, <logosinus.get(i)
logoxinehi:
.fill $100, >logosinus.get(i)

.var siney = List().add(
 38,37,36,35,34,33,32,31,30,29,28,27,26,26,25,24
,23,22,21,20,20,19,18,17,16,16,15,14,13,13,12,11
,11,10,9,9,8,7,7,6,6,5,5,4,4,3,3,3
,2,2,2,1,1,1,1,0,0,0,0,0,0,0,0,0
,0,0,0,0,0,0,0,0,0,0,1,1,1,2,2,2
,3,3,3,4,4,5,5,6,6,7,7,8,8,9,10,10
,11,12,12,13,14,14,15,16,17,17,18,19,20,21,22,22
,23,24,25,26,27,28,29,30,31,31,32,33,34,35,36,37
,38,39,40,41,42,43,44,44,45,46,47,48,49,50,51,52
,53,53,54,55,56,57,58,58,59,60,61,61,62,63,63,64
,65,65,66,67,67,68,68,69,69,70,70,71,71,72,72,72
,73,73,73,74,74,74,75,75,75,75,75,75,75,75,75,75
,75,75,75,75,75,75,75,75,75,74,74,74,74,73,73,73
,72,72,72,71,71,70,70,69,69,68,68,67,66,66,65,64
,64,63,62,62,61,60,59,59,58,57,56,55,55,54,53,52
,51,50,49,49,48,47,46,45,44,43,42,41,40,39,38,38
)
scroll_multiplexer_sine_y:
.fill $100, <siney.get(i)

.align $100
colortab:
.for (var i = 0; i < 4; i++)
{
.byte $06,$06,$06,$0e,$06,$0e
.byte $0e,$06,$0e,$0e,$0e,$03
.byte $0e,$03,$03,$0e,$03,$03
.byte $03,$01,$03,$01,$01,$03
.byte $01,$01,$01,$03,$01,$01
.byte $03,$01,$03,$03,$03,$0e
.byte $03,$03,$0e,$03,$0e,$0e
.byte $0e,$06,$0e,$0e,$06,$0e
.byte $06,$06,$06,$00,$00,$00
.byte $06,$06,$06,$0e,$06,$0e
.byte $0e,$0e,$0e,$e
}

.align $100
logosrc:
.for (var i = 0; i < 12; i++)
{
    .fill 32, 00
    .import binary "gfx/screenram.bin",2+i*64,64
    .fill 32, 00
}

scroll_multiplexer_pos_x:
.for (var s = 0; s < scroller_max_sprites; s++)
{
    .fill 24, s*24+i
}

.encoding "screencode_upper"
text:
.text "        "
.text "HELLO EVERYBODY!  HAS IT REALLY BEEN A YEAR ALREADY?  SEEMS SO...    "
.text "WELL, I WANTED SOMETHING BETTER FOR THE ANIVERSARY PRODUCTION BUT I'VE GOT"
.text " A PROBLEM WITH MY EYES AND IT'S HARD TO SIT AT THE COMPUTER :P     "
.text " FUN FACT: THIS INTRO IS NTSC COMPATIBLE!   NOTHING SPECIAL THOUGH, IT JUST WORKED..."
.text " I HAD TO DISABLE THE FLD THOUGH, NOT ENOUGH RASTERTIME... 60 HZ SURE LOOKS SMOOTH :)"
.text " THIS INTRO ALSO FITS INTO 16KB AND HAS AN EXIT ON SPACE..."
.text "    OK QUICK CREDITS... MUSIC BY JEFF    SPRITE FONT BY BOKANOID    EVERYTHING ELSE BY GOLARA    "
.text "SUPER SPECIAL GREETZ AND THX TO EVERYONE IN DESIRE AND SAMAR !! THANKS TO PEOPLE WRITING ARTICLES AT "
.text "CODEBASE64.ORG, POSTERS ON CSDB FORUMS. MICHAEL STEIL, NINJA AND DEEKAY FOR THEIR SEMINARS ON YOUTUBE ABOUT C64!"
.text "THIS WOULD NOT BE POSSIBLE WITHOUT YOU!    HOPEFULLY WE'LL SEE EACHOTHER AT SOME COOL DEMO PARTY SOON :)"
.text "    I GOTTA GO, HAVE A NICE DAY! GOLARA SIGNS OUT...        WRAP                 "
.byte $00



* = * "code"

entry:
        sei
        lda #$35
        sta $01

        lda #$7f
        sta $dc0d
        sta $dd0d
        lda $dc0d
        lda $dd0d

        ldx #$ff
        txs

        lda #00
        jsr muzak.init

//detect if machine is pal or ntsc
w0:     lda $d012
w1:     cmp $d012
        beq w1
        bmi w0
        and #3

        cmp #3
        beq its_pal
//for ntsc we need to disable FLD and one colorscroll
//because there's not enough rastertime
        lda #NOP
        sta irq1_fld.ntsc_nop1+0
        sta irq1_fld.ntsc_nop1+1
        sta irq1_fld.ntsc_nop1+2
        jmp after_machine_detect
its_pal:
        lda #NOP
        sta irq_under_logo.pal_nop1+0
        sta irq_under_logo.pal_nop1+1
        sta irq_under_logo.pal_nop1+2
after_machine_detect:

        lda #<text
        sta scrollptr+0
        lda #>text
        sta scrollptr+1

        lda #32
        ldx #scroller_max_sprites-1
!:      sta scrollbuffer,x
        dex
        bpl !-

        lda #$00
        ldx #$00
!:
        sta $d800+$000,x
        sta $d800+$100,x
        sta $d800+$200,x
        sta $d800+$300,x
        inx
        bne !-


.const rasterlogo = List().add($09,$02,$08,$0a,$0f,$07,$01,$07,$0f,$0a,$08,$02,$09,$00)
        ldx #39
!:
        .for (var i = 0; i < 12; i++)
        {
            lda #rasterlogo.get(i)
            sta $d800 + 40*4 + i*40,x
        }
        dex
        bpl !-



        ldx #$00
!:
        txa
        sta scrollorder,x
        inx
        cpx #scroller_max_sprites
        bne !-

        jsr move_scroller_sprites

        lda #<irq1_fld
        sta $fffe
        lda #>irq1_fld
        sta $ffff
        lda #$2f
        sta $d012
        lda #$1b
        sta $d011

        //lda #$00
        //sta $7fff
        :handle_the_scroller1()

        lda #$64
        sta $d018
        lda #$02
        sta $dd00
        lda #$06
        sta $d021
        sta $d020

        lda #1
        sta $d019
        sta $d01a
        cli

mainloop:   jmp mainloop


rasterbarr1:
.byte $06,$08,$0a,$0f,$07,$01
rasterbarr:
.byte $00,$08,$0a,$0f,$07,$01

//irqs code.
irq1_fld:
{
        ldx fld_idx:#$40
        ldy fld_tab, x
        //this will be nop'd on ntsc
        ntsc_nop1:inc fld_idx
fldloop:
        lda $d012
        clc
        adc #2
        and #7
        ora #$18
        sta $d011
        lda $d012
!:      cmp $d012
        beq !-
        dey
        bne fldloop
afterfld:
        lda #$ff
        sta $d015
        //setup first 8 sprites for multiplexer
        lda fld_tab,x
        clc
        adc #$38+(3*8)
        sta spriteyoffset
         .for (var i = 0; i < 8; i++)
        {
            ldx scrollorder+i
            lda scrollypos,x
            clc//maybe this clc is not needed
            adc spriteyoffset
            sta $d001 + i*2
            lda scrollxpos,x
            sta $d000 + i*2
            ldy #(1<<i)
            sty handle_d010.setbit
            jsr handle_d010
            
            lda scrollbuffer,x
            sta screenptr+$3f8 + i
        }
        //call the music
        //jsr muzak.play

        ldy #5
        lax spriteyoffset
        axs #7
        lda rasterbarr1,y
!:      cpx $d012
        bne !-
        //do the rasters
raster1loop1:
        sta $d020
        sta $d021
        
        pha
        pla
        pha
        pla
        pha
        pla
        pha
        pla
        pha
        pla

        nop
        nop
        nop

        dey
        lda rasterbarr1,y
        iny
        dey
        bpl raster1loop1
        //set d016 for logo scroll
        lda logo_softx:#$00
        sta $d016
        

        //setup irq for sprite multiplexer
        ldx scrollorder+8
        lda scrollypos,x
        clc
        adc spriteyoffset
        tax
        dex
        dex
        stx $d012
        lda #<irq_multiplex
        sta $fffe
        lda #>irq_multiplex
        sta $ffff

        inc $d019
        //lda rega:#$00
        //ldx regx:#$00
        //ldy regy:#$00
        rti
}
irq_multiplex:
{
        //set sprite registers
        .for (var i = 0; i < 7; i++)
        {
            ldx scrollorder+i+8
            lda scrollypos,x
            clc//maybe not needed
            adc spriteyoffset
            sta $d001 + i*2
            lda scrollxpos,x
            sta $d000 + i*2
            ldy #(1<<i)
            sty handle_d010.setbit
            jsr handle_d010
            
            lda scrollbuffer,x
            sta screenptr+$3f8 + i
        }

        //setup irq below logo.
        lda spriteyoffset
        clc
        adc #(12*8)
        sta $d012
        lda #<irq_under_logo
        sta $fffe
        lda #>irq_under_logo
        sta $ffff

        jsr muzak.play

        inc $d019
        rti
}

irq_under_logo:
{
//turn off xscroll
        lda #$00
        sta $d016
        lda #$00
        sta $d015
        //do the rasters
        ldy #5
        lda rasterbarr,y

        ldx $d012
!:      cpx $d012
        beq !-

        
raster1loop1:
        sta $d020
        sta $d021
        
        pha
        pla
        pha
        pla
        pha
        pla
        pha
        pla
        pha
        pla

        nop
        nop
        nop

        dey
        lda rasterbarr,y
        iny
        dey
        bpl raster1loop1

        //"raster" code is done
        //now the calculations
        jsr move_scroller_sprites

        ldx logoindex:#$00
        lda logoxinelo,x
        lsr
        lsr
        lsr
        ldy logoxinehi,x
        clc
        adc logochartab,y
        sta copy_logo_data1.logo_copy_idx
        lda logoxinelo,x
        eor #$ff
        and #7
ee:
        sta irq1_fld.logo_softx
        jsr moveleft
        inc logoindex

        lda #<irq1_fld
        sta $fffe
        lda #>irq1_fld
        sta $ffff
        lda #$37
        sta $d012
        lda #$18
        sta $d011

        ldx color_index:#$00
        ldy #39

top_one:
!:
        lda colortab,x
        .for (var i = 1; i < 3; i++)
        {
            sta $d800 + i*40,y
        }
        inx
        dey
        bpl !-

        ldx color_index
        ldy #39
//this will be nop'd on pal
        pal_nop1: jmp skip_bottom

bottom_oone:        
!:
        lda colortab,x
        .for (var i = 17; i < 19; i++)
        {
            sta $d800 + i*40,y
        }
        dex
        dey
        bpl !-
skip_bottom:
        inc color_index

//read keyboard, if space pressed, do reset
        lda #$00
        sta $dc03
        lda #$ff
        sta $dc02
        lda #$7f
        sta $dc00
        lda $dc01
        and #$10
        bne no_space
        //bank in kernel/basic and do reset
        lda #$37
        sta $01
        jmp $fce2
no_space:
        inc $d019
        rti
}
logochartab:
.byte 00+39, 32+39, 64+39


sort:
{
    ldx #$00
loop1:ldy scrollorder+1,x
loop2:lda scrollypos,y
    ldy scrollorder,x
    cmp scrollypos,y
    bcc swap
    inx
    cpx #scroller_max_sprites-1
    bne loop1
    beq end
swap:
    lda scrollorder+1,x
    sta scrollorder+0,x
    sty scrollorder+1,x
    tay
    dex
    bpl loop2
    inx
    beq loop1
end:
    rts
}
move_scroller_sprites:
{
    ldx scroller_pox_x:#24
    dex
    dex
    bpl still_going_left

    .for (var i = 0; i < scroller_max_sprites-1; i++)
    {
        lda scrollbuffer+i+1
        sta scrollbuffer+i+0
    }
    ldy #$00
    lda (scrollptr),y
    bne !+
    lda #<text
    sta scrollptr+0
    lda #>text
    sta scrollptr+1
    lda (scrollptr),y
!:
    sta scrollbuffer+scroller_max_sprites-1
    inc scrollptr+0
    bne !+
    inc scrollptr+1
!:
    lax scroller_y_pos
    axs #-16
    stx scroller_y_pos
    ldx #24
    stx scroller_pox_x
    dex
still_going_left:
    .for (var i = 0; i < scroller_max_sprites; i++)
    {
        lda scroll_multiplexer_pos_x+i*24,x
        sta scrollxpos+i
    }
    stx scroller_pox_x

    ldx scroller_y_pos:#$00
    .for (var i = 0; i < scroller_max_sprites; i++)
    {
        lda scroll_multiplexer_sine_y,x
        sta scrollypos + i
        txa
        axs #-16
    }  
    inc scroller_y_pos
    inc scroller_y_pos

    jsr sort

    rts
}

moveleft:
copy_logo_data1:
{
    ldx logo_copy_idx:#39
    ldy #39
!:
    .for (var i = 0; i < 12; i++)
    {
        lda logosrc + i*128, x
        sta screenptr + 4*40 + i*40, y
    }
    dex
    dey
    bpl !-

    rts
}

handle_d010:
{
        cpx #10
        bcc clear
        cmp #104
        bcs clear

        lda $d010
        ora setbit:#$00
        sta $d010
end:
        rts
clear:
        lda setbit
        eor #$ff
        and $d010
        sta $d010
        rts
}

.macro handle_the_scroller1()
{
    lda #$ff
    sta $d01c
    lda #$00
    sta $d025
    lda #$0f
    sta $d026
    lda #$01
    .for (var i = 0; i < 8; i++)
    {
        sta $d027+i
    }
}

* = $7200 "SID"
.fill muzak.size, muzak.getData(i)
